<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Logic Captcha</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <script src="http://cdnjs.cloudflare.com/ajax/libs/paper.js/0.22/paper.js"></script>
    <script src="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.2.2/js/bootstrap.min.js"></script>
    <link href="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.2.2/css/bootstrap-combined.min.css" rel="stylesheet">
  </head>

  <body>
<div id="wrap">

<div class="navbar">
  <div class="navbar-inner">
    <a class="brand" href="">Logicaptcha</a>
    <ul class="nav">
      <li class="active"><a href="object.php">Object Identification</a></li>
	  <li><a href="art.php">Adjective</a></li>
    </ul>
  </div>
</div>
	<div class="container">
		<form class="form-horizontal" action="update.php" method= "post" >
		
		<h2>
			Please draw the outline of a [<script language="JavaScript">
					KeywordArray = new Array(7);  
					KeywordArray[0] = "hamburger ";  
					KeywordArray[1] = "man ";  
					KeywordArray[2] = "mailbox ";  
					KeywordArray[3] = "fire hydrant";  
					randno = Math.floor ( Math.random() * KeywordArray.length ); 
					document.write(KeywordArray[randno]);
			</script>]
		</h2>
				<canvas id="myCanvas" width="800" height="500" class="img img-rounded"></canvas>
			
			
			<script type="text/javascript">
			paper.install(window);
			// Keep global references to both tools, so the HTML
			// links below can access them.
			var tool1, tool2;
		
			window.onload = function() {
				paper.setup('myCanvas');

				// Create two drawing tools.
				// tool1 will draw straight lines,
				// tool2 will draw clouds.

				// Both share the mouseDown event:
				var path;
				function onMouseDown(event) {
					path = new Path();
					path.strokeColor = 'black';
					path.add(event.point);
				}

				tool1 = new Tool();
				tool1.onMouseDown = onMouseDown;

				tool1.onMouseDrag = function(event) {
					path.add(event.point);
				}
					tool1.activate();
			}
		</script>
			
			
			<br><br><br>
		<input type="text" class="span2" name= "answer" size="30" />
		<button type="submit" value="Submit"  class="btn btn-large btn-primary"  >Submit</button> <br><br>
		
		</form>


		<div id="push"></div>

		<div id="footer">
			<div class="container">
			<a href="http://brianfitzgerald.site44.com" class="muted credit"> Brian Fitzgerald </a>
		</div>
		</div>
		</div>
		</div>

  </body>
</html>
