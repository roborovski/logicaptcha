<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Logic Captcha</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <script src="http://cdnjs.cloudflare.com/ajax/libs/paper.js/0.22/paper.js"></script>
    <script src="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.2.2/js/bootstrap.min.js"></script>
    <link href="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.2.2/css/bootstrap-combined.min.css" rel="stylesheet">
  </head>

  <body>
<div id="wrap">

<div class="navbar">
  <div class="navbar-inner">
    <a class="brand" href="">Logicaptcha</a>
    <ul class="nav">
      <li><a href="object.php">Object Identification</a></li>
	  <li class="active"><a href="art.php">Adjective</a></li>
    </ul>
  </div>
</div>
	<div class="container">
		<form class="form-horizontal" action="update.php" method= "post" >
		Please write the word that corresponds to the best adjective to fill in the blank in the sentence below.
		<div class="well">
		<h2>
			<script language="JavaScript">
					KeywordArray = new Array(7);  
					KeywordArray[0] = "The water ";  
					KeywordArray[1] = "The pavement ";  
					KeywordArray[2] = "The hamburger ";  
					KeywordArray[3] = "The salamander";  
					randno = Math.floor ( Math.random() * KeywordArray.length ); 
					document.write(KeywordArray[randno]);
			</script>
			felt ________.
		</h2></div>
		<table class="table table-striped">
		<tr>
		<td><img src="captcha/captcha_1.gif" class="img-polaroid"></td>
		<td>hard</td>
		</tr>
		<tr>
		<td><img src="captcha/captcha_2.gif" class="img-polaroid"></td>
		<td>raw 2</td>
		</tr>
		<tr>
		<td><img src="captcha/captcha_3.gif" class="img-polaroid"></td>
		<td>soft</td>
		</tr>
		<tr>
		<td><img src="captcha/captcha_4.gif" class="img-polaroid"></td>
		<td>cold</td>
		</tr>
		<tr>
		<td><img src="captcha/captcha_5.gif" class="img-polaroid"></td>
		<td>hot to the touch</td>
		</tr>
		<tr>
		<td><img src="captcha/captcha_6.gif" class="img-polaroid"></td>
		<td>gristly</td>
		</tr>

		</table>

		<input type="text" class="span2" name= "answer" size="30" />
		<br><br>
		<br><br>
		<button type="submit" value="Submit"  class="btn btn-large btn-primary"  >Submit</button> <br><br>
		
		</form>


		<div id="push"></div>

		<div id="footer">
			<div class="container">
			<a href="http://brianfitzgerald.site44.com" class="muted credit"> Brian Fitzgerald </a>
		</div>
		</div>
		</div>
		</div>

  </body>
</html>
