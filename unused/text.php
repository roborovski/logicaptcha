<?php
// check that form was submitted
//   (you'll need to change these indices to match your form field names)
if( !empty( $_POST['answer'] ) && !empty( $_POST['survey'] ) ){
    // remove html tags from submission
    //   (since you don't want them)
    $firstname = strip_tags( $_POST['firstname'] );
    $lastname = strip_tags( $_POST['lastname'] );
    // create the date
    //   (you can change the format as desired)
    $date = date( 'Y-m-d' );
    // create an array that holds your info
    $record = array( $firstname,$lastname,$date );
    // save the record to your .txt file (I still recommend JSON)
    $json = json_encode( $record );
    $file = 'records.txt';
    file_put_contents( $json,$file );
}