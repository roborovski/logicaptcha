<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Logic Captcha</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <script src="http://cdnjs.cloudflare.com/ajax/libs/paper.js/0.22/paper.js"></script>
    <script src="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.2.2/js/bootstrap.min.js"></script>
    <link href="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.2.2/css/bootstrap-combined.min.css" rel="stylesheet">
  </head>

  <body>
<div id="wrap">

<div class="navbar">
  <div class="navbar-inner">
    <a class="brand" href="#">Logicaptcha</a>
    <ul class="nav">
      <li><a href="index.php">Original</a></li>
	  <li><a href="art.php">Art</a></li>
      <li class="active"><a href="translate.php">Translation</a></li>
    </ul>
  </div>
</div>
	<div class="container">
        <h2>Logicaptcha: English Edition</h2><br>
			<div class="well">
				<div class="hero-unit">
				<legend>Using the word definitions provided, can you learn enough about the language shown below to translate English to it? </legend>
				</div>
				<table class="table table-striped">
				<tr><th>Agta Word</th><th>English Translation</th></tr>
				<tr><td>wer</td><td>'creek'</td></tr>
				<tr><td>balabahuy</td><td>'little pig'</td></tr>
				<tr><td>talobag</td><td>'beetle'</td></tr>
				<tr><td>bakbakat</td><td>'granny'</td></tr>
				<tr><td>palapirak</td><td>'little money'</td></tr>
				<tr><td>bahuy</td><td>'pig'</td></tr>
				<tr><td>bag</td><td>'loincloth'</td></tr>
				<tr><td>walawer</td><td>'little creek'</td></tr>
				<tr><td>balabag</td><td>'little loincloth'</td></tr>
				<tr><td>takki</td><td>'leg'</td></tr>
				<tr><td>labang</td><td>'patch'</td></tr>
				</table>     

		<form class="form-horizontal" action="updatetranslate.php" method= "post" > <br><br>
		 'little leg' to English:  <input type="text" class="span2" name= "agta1" size="30" /><br><br>
		 'money' to English:  <input type="text" class="span2" name= "agta2" size="30" /><br><br>
		 'little beetle' to English:  <input type="text" class="span2" name= "agta3" size="30" /><br><br>
		 'little pig' to English:  <input type="text" class="span2" name= "agta4" size="30" /><br><br>
		 </div>
		 <div class="well">
		<div class="hero-unit">
		<legend>What words best complete the following sentence?</legend>
		
		
		
		<h4>The rain in Spain mainly falls on the ______.</h4><input type="text" class="span2" name= "sentence1" size="30" /><br>
		<h4>It was a dark and _____ night.</h4><input type="text" class="span2" name= "sentence2" size="30" /><br>
		<h4></h4>
		</div>
		<h1>
		
		
		
		
		
		</div>
		<button type="submit" value="Submit" class="btn btn-large btn-primary">Submit</button> <br><br>
		</form>
		<div id="push"></div>

				<div id="footer">
					<div class="container">
					<a href="http://brianfitzgerald.site44.com" class="muted credit"> Brian Fitzgerald </a>
					</div>
				</div>
	</div>
</div>

  </body>
</html>
